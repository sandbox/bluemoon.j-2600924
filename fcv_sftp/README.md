ccassionally you might need to SFTP data into remote server,
for example after generating data feed etc.

This project brings phpseclib SFTP library into Drupal, provide configuration
forms, with FCV SFTP, now you can handle SFTP with just PHP,
there is no PHP FTP extension needed.

To start, you need to download phpsecclib from
http://phpseclib.sourceforge.net/, put it under libraries folder,
and rename the folder to phpseclib. For example: sites/all/libraries/phpseclib/

Then go to /admin/config/sftp config SFTP account.
Finally in your own code, invoke function fcv_sftp_send_data() to send data
to remote SFTP server.
